import React, { ChangeEvent, Fragment, useState } from "react";
import ActorComponent from "./components/Actor/ActorComponent";
import "./App.css";

function App() {
    const [state, setState] = useState({
        nActors: 20,
        interval: 3000,
        rowSize: 4,
        b: 0.8341,
        epsilon: 0.7382,
    } as { [key: string]: number });

    const handleStartRandomBlink = () => {
        window.dispatchEvent(new Event("StartRandomBlink"));
    };
    const handleStartSync = () => {
        window.dispatchEvent(new Event("StartSync"));
    };
    const handleStateChange = (key: string) => (
        e: ChangeEvent<HTMLInputElement>
    ) => {
        const newVal = e.currentTarget.value;
        setState((state) => {
            const newNumber = parseFloat(newVal) || 0;
            state[key] = newNumber;
            return { ...state };
        });
    };

    return (
        <div className="App">
            <div
                className="ActorsContainer"
                style={{
                    gridTemplateColumns: `repeat(${state.rowSize || 1}, 1fr)`,
                }}
            >
                {Array(state.nActors || 0)
                    .fill(0)
                    .map((_, i) => (
                        <ActorComponent
                            key={i}
                            number={i}
                            interval={state.interval}
                            b={state.b}
                            epsilon={state.epsilon}
                            totalCells={state.nActors}
                            rowSize={state.rowSize}
                        />
                    ))}
            </div>
            <div className="ControlsContainer">
                {Object.entries(state).map(([key, value], i) => {
                    return (
                        <Fragment key={i}>
                            <label> {key}: </label>
                            <input
                                type="number"
                                value={value}
                                onChange={handleStateChange(key)}
                            />
                        </Fragment>
                    );
                })}

                <br />
                <button onClick={handleStartRandomBlink}>
                    Start Random Blink
                </button>
                <button onClick={handleStartSync}>Start Sync</button>
            </div>
        </div>
    );
}

export default App;
