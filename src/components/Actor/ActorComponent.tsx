import React, { useCallback, useEffect, useRef, useState } from "react";
import "./styles.css";

interface ActorComponentProps {
    number: number;
    interval: number;
    b: number;
    epsilon: number;
    totalCells: number;
    rowSize: number;
}

const sleep = (ms: number) => new Promise((r) => setTimeout(r, ms));

const ActorComponent = (props: ActorComponentProps) => {
    const number = props.number;
    const interval = props.interval;
    const b = props.b;
    const epsilon = props.epsilon;
    const totalCells = props.totalCells;
    const rowSize = props.rowSize;

    let phase = useRef(interval);
    // console.log(number);
    const [blinkStyle, setBlinkStyle] = useState({
        background: "inherit",
    });

    const getNeighborNumbers = (selfNumber: number) => {
        const neighborNumbers = [];
        neighborNumbers.push(selfNumber + 1);
        neighborNumbers.push(selfNumber - 1);
        neighborNumbers.push(selfNumber + rowSize);
        neighborNumbers.push(selfNumber - rowSize);
        return neighborNumbers.filter((n) => n < totalCells && n >= 0);
    };

    const neighborNumbers = getNeighborNumbers(number);

    const actorLog = useCallback(
        (...args: any) => {
            console.log(`Actor ${number}: `, ...args);
        },
        [number]
    );
    const sendBlinkEvent = useCallback(() => {
        window.dispatchEvent(
            new CustomEvent("blink", {
                detail: {
                    actorNumber: number,
                },
            })
        );
    }, [number]);

    const handleNeighborBlink = useCallback(
        (e: any) => {
            const eventActorNumber = e.detail.actorNumber;
            if (neighborNumbers.includes(eventActorNumber)) {
                const alpha = Math.exp(b * epsilon);
                const beta = (alpha - 1) / (Math.exp(b) - 1);
                phase.current = Math.min(
                    alpha * phase.current + beta,
                    interval
                );
                // phase.current += 0.2 * phase.current;
                actorLog("neighborBlinked");
            }
        },
        [phase, b, epsilon, neighborNumbers, actorLog, interval]
    );

    const blink = useCallback(() => {
        sendBlinkEvent();
        setBlinkStyle(() => {
            return {
                background: "green",
            };
        });
        setTimeout(() => {
            setBlinkStyle(() => {
                return {
                    background: "inherit",
                    transition: "background 1s linear",
                };
            });
        }, 100);
    }, [sendBlinkEvent]);

    const handleStartBlink = useCallback(async () => {
        actorLog("start random blink acknowleged");
        const randomInterval = interval * Math.random();
        await sleep(randomInterval);
        while (true) {
            if (isNaN(phase.current)) {
                phase.current = interval;
            }

            if (phase.current >= interval) {
                blink();
                phase.current = 0;
            } else {
                phase.current += 10;
            }
            // if (number === 12) {
            //     actorLog(phase);
            // }
            await sleep(10);
        }
    }, [phase, actorLog, blink, interval]);

    useEffect(() => {
        window.addEventListener("StartRandomBlink", handleStartBlink);
        const startSync = () => {
            window.addEventListener("blink", (e) => {
                handleNeighborBlink(e);
            });
        };
        window.addEventListener("StartSync", startSync);
        return () => {
            window.removeEventListener("StartRandomBlink", handleStartBlink);
            window.removeEventListener("StartSync", startSync);
            window.removeEventListener("blink", handleNeighborBlink);
        };
    }, [handleNeighborBlink, handleStartBlink]);
    return (
        <div className="ActorComponent" style={blinkStyle} onClick={blink}>
            <p>Actor {number}</p>
        </div>
    );
};

export default ActorComponent;
